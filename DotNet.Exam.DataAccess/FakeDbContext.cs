﻿using DotNet.Exam.Model;

namespace DotNet.Exam.DataAccess;

public static class FakeDbContext
{
	public static IList<User> Users { get; } = new List<User>();
}