﻿using HotChocolate;

namespace DotNet.Exam.Model;

public class User
{
	public required int Id { get; set; }

	[GraphQLNonNullType]
	public required string FirstName { get; set; }

	[GraphQLNonNullType]
	public required string LastName { get; set; }

	public required UserRole Role { get; set; }
	
	public required int Age { get; set; }
	
	public bool IsBanned { get; set; } = false;
}