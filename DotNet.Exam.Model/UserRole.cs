namespace DotNet.Exam.Model;

public enum UserRole
{
	User,
	Admin
}