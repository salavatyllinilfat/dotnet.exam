using Core;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddGraphQL(SchemaBuilder.New().AddQueryType<Query>().AddMutationType<Mutation>().Create());

var app = builder.Build();

app.MapGraphQL();

app.Run();