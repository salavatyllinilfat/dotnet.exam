﻿using DotNet.Exam.DataAccess;
using DotNet.Exam.Model;
using HotChocolate;

namespace Core;

public class Mutation
{
	private int _idCounter;

	public Task<User> CreateUser(string firstName, string lastName, int age)
	{
		var user = new User
		{
			Id = Interlocked.Increment(ref _idCounter),
			FirstName = firstName,
			LastName = lastName,
			Role = UserRole.User,
			Age = age
		};

		FakeDbContext.Users.Add(user);
		
		return Task.FromResult(user);
	}
	
	public Task<User> UpdateUser(int id, string firstName, string lastName, int age)
	{
		var user = FakeDbContext.Users.FirstOrDefault(x => x.Id == id)!;

		user.FirstName = firstName;
		user.LastName = lastName;
		user.Age = age;
		
		return Task.FromResult(user);
	}
	
	public Task<User> DeleteUser(int id)
	{
		var user = FakeDbContext.Users.FirstOrDefault(x => x.Id == id)!;
		
		FakeDbContext.Users.Remove(user);
		
		return Task.FromResult(user);
	}
}