using DotNet.Exam.DataAccess;
using HotChocolate;
using DotNet.Exam.Model;

namespace Core;

public class Query
{
	[GraphQLNonNullType]
	public List<User> GetUsers () => FakeDbContext.Users.ToList(); 
    
	public User? GetUser(int id) => FakeDbContext.Users.FirstOrDefault(x => x.Id == id);
}